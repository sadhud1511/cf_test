using {NorthWind as external} from './external/NorthWind.csn';

service CatalogService {

    @readonly
    entity Products as projection on external.Products {
        key ID, Name, Description, ReleaseDate, DiscontinuedDate, Rating, Price
    };

}

service TestService {

    @readonly
    entity HanaTest as projection on external.Products {
        key ID, Name, Description, ReleaseDate, DiscontinuedDate, Rating, Price
    };
    action getDataFromNeo() returns String;

}