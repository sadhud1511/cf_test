const cds = require('@sap/cds');
//require the cdse module for local testing of the destination from SCP
const cdse = require('cdse');

module.exports = cds.service.impl(async function() {
	const { Products } = this.entities;
	//const service = await cds.connect.to('NorthWind');
	/*this.on('READ', Products, request => {
		return service.tx(request).run(request.query);
    });*/
    
    //Function to write custom exit for Nodejs
    const service = await cdse.connect.to('NorthWind');
    const testhanaservice = await cdse.connect.to("CF_Test");
    this.on('READ', Products, async () => {
		const result = await service.run({ url: 'Products' });
		return result.value;
    });
    
    this.on('TestService.getDataFromNeo', async req => {
        const retData = await testhanaservice.run({ url: '/pvt/CF_TEST/CF_Test.xsodata/CF_Test' });
           return retData.value;
        })
});